<?php

class input_model extends CI_ModeL
{

    public function insert_data_jawaban($table, $data)
    {
        $this->db->insert($table, $data);
    }
    public function insert_data_mahasiswa($data)
    {
        $this->db->insert("mahasiswa", $data);
    }
    public function fetch_data2()
    {
        //$query = $this->db->get("admin");
        $query = $this->db->query("SELECT * FROM pertanyaan ORDER BY id_pertanyaan ASC");
        return $query;
    }

    public function count($table)
    {
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function hitungProdi($where)
    {
        $this->db->select('COUNT(npm) as prodi');
        $this->db->from('mahasiswa');
        $this->db->where('id_prodi', $where);
        $query = $this->db->get();
        foreach ($query->result() as $r) {
            return $r->prodi;
        }
    }

    public function hitungFakultas($where)
    {
        $this->db->select('COUNT(npm) as prodi');
        $this->db->from('mahasiswa');
        $this->db->where('id_fakultas', $where);
        $query = $this->db->get();
        foreach ($query->result() as $r) {
            return $r->prodi;
        }
    }


    public function hasil()
    {
        return $this->db->get('hasil');
    }


    public function getnpm()
    {
        $query = $this->db->query("SELECT DISTINCT npm FROM hasil;");
        return $query;
    }

    public function getnpmwhere($npm)
    {
        $query = $this->db->query("SELECT * FROM `hasil` JOIN mahasiswa ON mahasiswa.npm = hasil.npm WHERE hasil.npm = '$npm'");
        return $query;
    }

    public function getnpmwhereessay($npm)
    {
        $query = $this->db->query("SELECT * FROM `jwb_essay` JOIN mahasiswa ON mahasiswa.npm = jwb_essay.npm WHERE jwb_essay.npm = '$npm'");
        return $query;
    }

    public function pertanyaan()
    {
        return $this->db->get('pertanyaan');
    }

    public function getdatapertanyaanjwb()
    {
        $this->db->join('hasil b', 'b.id_pertanyaan= a.id_pertanyaan');
        $this->db->where('b.npm', '17.0504.0078');
        return $this->db->get('pertanyaan a')->result_array();
    }

    public function datatable($table)
    {
        return $this->db->get($table);
    }

    public function getnilai($npm, $id)
    {
        $query = $this->db->query("SELECT count(id_hasil) AS hasil FROM `hasil`WHERE id_pertanyaan = '$id' AND jawaban ='$npm'");
        return $query;
    }

    public function delete()
    {
        $this->db->truncate('hasil');
        $this->db->truncate('jwb_essay');
        $this->db->empty_table('mahasiswa');
    }

    // SELECT * FROM `hasil`WHERE id_pertanyaan = '8'AND jawaban ='baik';
}
