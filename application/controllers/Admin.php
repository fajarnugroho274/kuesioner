<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Admin extends CI_Controller {

    
    public function index()
    {
        if ($this->session->userdata('username') != '') {
            $this->load->model("input_model");
            $jumProdi = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'];
            $jumFakultas = ['1', '2', '3', '4', '5', '6', '7'];
            for ($i = 0; $i < 21; $i++) {
                $mhs = $jumProdi[$i];
                $data['hslProdi'][$i] = $this->input_model->hitungProdi($mhs);
            }
            for ($i = 0; $i < 7; $i++) {
                $mhs = $jumFakultas[$i];
                $data['hslFakultas'][$i] = $this->input_model->hitungFakultas($mhs);
            }
            $data['prodi'] =  $this->input_model->datatable('prodi')->result_array();
            $data['fakultas'] =  $this->input_model->datatable('fakultas')->result_array();
            $data['title'] =  "judul";
            $this->load->view("admin", $data);
        } else {
            redirect(base_url() . 'Admin/login');
        }
    }
  
    function login()
    {
        $this->load->view("login");
    }


    function login_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', "username", 'required');
        $this->form_validation->set_rules('password', "password", 'required');
        if($this->form_validation->run())
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $this->load->model('login_model');
            if($this->login_model->can_login($username, $password))
            {
                $session_data = array(
                    'username' => $username
                );
                $this->session->set_userdata($session_data);
                redirect(base_url() . 'Admin');
            }
            else
            {
                $this->session->set_flashdata('error', 'username dan password salah');
                redirect(base_url() . 'Admin/login');
            }
        }
        else
        {
            $this->login();
        }

    }

    function logout()
    {
        $this->session->unset_userdata('username');
        redirect(base_url() . 'Admin/login');
    }
}