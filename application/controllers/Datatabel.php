<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Datatabel extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
 
        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */ 
 
        $this->load->library('grocery_CRUD');
 
    }

    public function _example_output($output = null)
	{
		if($this->session->userdata('username') !='')
        {
            
            $this->load->view('admin_data.php',(array)$output);
            
        }
        else
        {
            redirect(base_url() . 'Admin/login');
        }
    }
    
    public function data($data){
            $this->load->view("admin_data", $data);
    }
 
   

    function login()
    {
        $this->load->view("login");
    }


    function login_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', "Username", 'required');
        $this->form_validation->set_rules('password', "Password", 'required');
        if($this->form_validation->run())
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $this->load->model('login_model');
            if($this->login_model->can_login($username, $password))
            {
                $session_data = array(
                    'username' => $username
                );
                $this->session->set_userdata($session_data);
                redirect(base_url() . 'Admin');
            }
            else
            {
                $this->session->set_flashdata('error', 'username dan password salah');
                redirect(base_url() . 'Admin/login');
            }
        }
        else
        {
            $this->login();
        }

    }
    public function index(){
        if($this->session->userdata('username') !='')
        {
            
            $this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
            
        }
        else
        {
            redirect(base_url() . 'Admin/login');
        }
    }
    function logout()
    {
        $this->session->unset_userdata('username');
        redirect(base_url() . 'Admin/login');
    }
 
    public function user()
    {
        
        $crud = new grocery_CRUD();
        $crud->set_table('admin');
        $output = $crud->render();
 
        $this->_example_output($output);    
    }



    public function datamahasiswa()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('mahasiswa');
        $crud->columns('npm','nama','nomor_handphone','id_prodi','id_fakultas');
        $crud->display_as('id_prodi','Nama Prodi');
        $crud->display_as('id_fakultas', 'Nama Fakultas');
        $crud->set_relation('id_fakultas', 'fakultas', 'nama_fakultas');
        $crud->set_relation('id_prodi', 'prodi', 'nama_prodi');
        $output = $crud->render();
 
        $this->_example_output($output);        
    }

    public function datapertanyaan()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('pertanyaan');
        $crud->columns('pertanyaan','jwb1','jwb2','jwb3','jwb4','jwb5');
        $output = $crud->render();
 
        $this->_example_output($output);              
    } 

    //public function datapertanyaanessay()
    //{
    //   $crud2 = new grocery_CRUD();
    //   $crud2->set_table('essay');
    //  $crud2->columns('pertanyaan');
    //  $output2 = $crud2->render();

    //    $this->_example_output($output2);        
    //} 
    
    // public function hasil()
    // {
    //     $crud = new grocery_CRUD();
    //     $crud->set_table('hasil');
    //     $crud->set_relation('id_pertanyaan','pertanyaan','pertanyaan');
    //     $crud->set_relation('npm','mahasiswa','nama');
    //     $output = $crud->render();
        
    //     $this->_example_output($output);   
    // } 

    public function hasil()
    {
        $this->load->model("input_model");
        $array['per'] = $this->input_model->count('pertanyaan');
        $array['peressay'] = $this->input_model->count('essay');
        $array['npm'] = $this->input_model->getnpm()->result_array();
        $array['data'] = $this->input_model->hasil()->result_array();
        if (empty($array['data'])) {
             $array['n'] = null;
        } else{
            $c =count($array['npm']);
            for ($i=0; $i <$c ; $i++) { 
            $npm = $array['npm'][$i]['npm'];
            $array['n'][$i] = $this->input_model->getnpmwhere($npm)->result_array();
            $array['ne'][$i] = $this->input_model->getnpmwhereessay($npm)->result_array();
            }
        }
        $array['npm'] = $this->input_model->getnpm()->result_array();
        $this->load->view("hasilku",$array);
    } 

    //public function hasilgrafik($id = null)
    //{
      //  $this->load->model("input_model");
      //  $data["jumper"] =  $this->input_model->count('pertanyaan');
      //  $data['pertanyaan'] =  $this->input_model->datatable('pertanyaan')->result_array();
       // $jbw = ['Sangat Puas', 'Puas', 'Cukup Puas', 'Tidak Puas', 'Sangat Tidak Puas'];
       // for ($i=0; $i <5; $i++) { 
         //  $data[$i] =  $this->input_model->getnilai($jbw[$i], $id)->result_array();
         //  $data['kkk'][$i] = $hasilku[$i] = $data[$i][0];
        //}
        //$this->load->view("hasilgrafik",$data);
    //} 

    public function carigrafik($id = null)
    {
        $this->load->model("input_model");
        $jbw = ['Sangat baik', 'Baik', 'Netral', 'Kurang', 'Sangat Kurang'];
        // echo $jbw[4];
        for ($i=0; $i <5; $i++) { 
            // echo $jbw[$i]."<br>";
           $data[$i] =  $this->input_model->getnilai($jbw[$i], $id)->result_array();
           $a['kkk'][$i] = $hasilku[$i] = $data[$i][0];
        }
        $this->load->view("c",$a);
       
        // echo "<pre>";print_r($a);
        // echo count($data);
        // die;
    }

    public function deleteAll()
    {
        $this->load->model("input_model");
        $this->input_model->delete();
        redirect('Datatabel/hasil');
    }

    
         

}