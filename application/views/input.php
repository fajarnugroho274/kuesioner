<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Sistem Informasi Kuisioner Survey</title>
  <link href="assetsh/gambar/Bg3.png" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url() ;?>/assetsh/theme/bootstrap.css" media="screen">
  <link rel="stylesheet" href="<?php echo base_url() ;?>/assetsh/theme/usebootstrap.css">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="bootstrap/html5shiv.js"></script>
      <script src="bootstrap/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo base_url() ;?>/" class="navbar-brand">Sistem Informasi Survey Kepuasan Mahasiswa</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
        </div>
      </div>
    </div>

    <div class="container">

      <div class="col-lg-12">
        <div class="page-header">
          <h1 id="forms"><center>Formulir Kuisioner Survey<center></h1>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" action="<?php echo base_url()?>input/form_validation" method="post">
              <div class="form-group">
                <label for="inputNPM" class="col-lg-2">NPM</label>
                <div class="col-lg-12">
                  <input type="text" class="form-control" name="npm">
                  <?= form_error('npm', '<small class="text-danger">', '</small>'); ?>
                  <div class="form-group">
                    <label for="inputNama" class="col-lg-2">Nama</label>
                    <div class="col-lg-12">
                      <input type="text" class="form-control" name="nama"> 
                      <?= form_error('nama', '<small class="text-danger">', '</small>'); ?> 
                      <div class="form-group">
                        <label for="inputFakultas" class="col-lg-2">Fakultas</label>
                        <div class="col-lg-12">
                          <select name="fakultas" class="form-control">
                            <option value="" selected disabled>Pilih Nama Fakultas</option>
                            <option value="01">Fakultas Ekonomi dan Bisnis</option>
                            <option value="02">Fakultas Hukum</option>
                            <option value="03">Fakultas Keguruan dan Ilmu Pendidikan</option>
                            <option value="04">Fakultas Agama Islam</option>
                            <option value="05">Fakultas Teknik</option>
                            <option value="06">Fakultas Ilmu Kesehatan</option>
                            <option value="07">Fakultas Psikologi dan Humaniora</option>
                          </select>
                          <?= form_error('fakultas', '<small class="text-danger">', '</small>'); ?>
                          <div class="form-group">
                            <label for="inputProdi" class="col-lg-2">Prodi</label>
                            <div class="col-lg-12">
                              <select name="prodi" class="form-control">
                                <option value="" selected disabled>Pilih Nama Prodi</option>
                                <option value="1">Manajemen (S-1)</option>
                                <option value="2">Akuntansi (S-1)</option>
                                <option value="3">Ilmu Hukum (S-1)</option>
                                <option value="4">Bimbingan Konseling (S-1)</option>
                                <option value="5">Pendidikan Guru PAUD (S-1)</option>
                                <option value="6">Pendidikan Guru SD (S-1)</option>
                                <option value="7">Pendidikan Agama Islam (S-1)</option>
                                <option value="8">Hukum Ekonomi Syariah (S-1)</option>
                                <option value="9">Pendidikan Guru Madrasah Ibtidaiyah (S-1)</option>
                                <option value="10">Manajemen Pendidikan Islam (S-2)</option>
                                <option value="11">Teknik Industri (S-1)</option>
                                <option value="12">Teknik Informatika (D-3)</option>
                                <option value="13">Teknik Otomotif (D-3)</option>
                                <option value="14">Teknik Informatika (S-1)</option>
                                <option value="15">Ilmu Keperawatan (S-1)</option>
                                <option value="16">Keperawatan (D-3)</option>
                                <option value="17">Farmasi (S-1)</option>
                                <option value="18">Farmasi (D-3)</option>
                                <option value="19">Profesi Ners</option>
                                <option value="20">Psikologi (S-1)</option>
                                <option value="21">Ilmu Komunikasi (S-1)</option>
                              </select>
                              <?= form_error('prodi', '<small class="text-danger">', '</small>'); ?>
                              <div class="form-group">
                                <label for="inputNoHP" class="col-lg-2">No HP</label>
                                <div class="col-lg-12">
                                  <input type="number" class="form-control" name="hp">
                                  <?= form_error('hp', '<small class="text-danger">', '</small>'); ?>
                                </div>
                              </div>
                            </div>
                          </div>

                          <?php $no=1;
                          foreach($fetch_data2->result() as $row)
                          {
                           ?>   
                           <fieldset>
                             <input type="text" hidden="" name="id_pertanyaan<?php echo $row->id_pertanyaan; ?>" value="<?php echo $row->id_pertanyaan; ?>">       
                             <div id="p<?php echo $row->id_pertanyaan; ?>" class="form-group">
                              <div class="col-lg-10">
                                <p><b><?php echo $row->pertanyaan; ?></b></p>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="jawaban<?php echo $row->id_pertanyaan; ?>" id="<?php echo $row->id_pertanyaan; ?>" value="<?php echo $row->jwb5; ?>">
                                    <?php echo $row->jwb5; ?>
                                  </label>
                                </div>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="jawaban<?php echo $row->id_pertanyaan; ?>"  value="<?php echo $row->jwb4; ?>">
                                    <?php echo $row->jwb4; ?>
                                  </label>
                                </div>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="jawaban<?php echo $row->id_pertanyaan; ?>" value="<?php echo $row->jwb3; ?>">
                                    <?php echo $row->jwb3; ?>
                                  </label>
                                </div>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="jawaban<?php echo $row->id_pertanyaan; ?>" value="<?php echo $row->jwb2; ?>">
                                    <?php echo $row->jwb2; ?>
                                  </label>
                                </div>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="jawaban<?php echo $row->id_pertanyaan; ?>"  value="<?php echo $row->jwb1; ?>">
                                    <?php echo $row->jwb1; ?>
                                  </label>
                                </div>
                                <?= form_error('jawaban'.$row->id_pertanyaan, '<small class="text-danger">', '</small>'); ?>                      
                              </div>              
                            </div>

                          </fieldset>
                        <?php } ?>

                        <?php $no=1;
                        foreach($essay->result() as $row)
                        {
                         ?>   
                         <fieldset>
                           <input type="text" hidden="" name="id_essay<?php echo $row->id_essay; ?>" value="<?php echo $row->id_essay; ?>">
                           <div class="form-group">
                            <div class="col-lg-12">
                              <p><b><?php echo $row->pertanyaan; ?></b></p>
                                <textarea name="jawabanessay<?php echo $row->id_essay; ?>" class="form-control" rows="3"></textarea>
                            </div>
                            <?= form_error('jawabanessay'.$row->id_essay, '<small class="text-danger">', '</small>'); ?>                      
                          </div>              
                      </fieldset>
                    <?php } ?>                 
                    <div class="form-group">
                      <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>   

                </div>
              </div>
              <p><a href="<?php echo base_url() ;?>" class="btn btn-info btn-lg">Kembali</a></p>
            </div>
            <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
            <script src="<?php echo base_url() ;?>/assetsh/bootstrap/bootstrap.min.js"></script>
            <script src="<?php echo base_url() ;?>/assetsh/bootstrap/usebootstrap.js"></script>
          </body>
          </html>