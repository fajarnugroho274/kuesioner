<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Sistem Informasi Kuisioner Survey</title>
  <link href="assetsh/gambar/Bg3.png" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url() ;?>/assetsh/theme/bootstrap.css" media="screen">
  <link rel="stylesheet" href="<?php echo base_url() ;?>/assetsh/theme/usebootstrap.css">
  <link rel= "stylesheet" type= "text/css" href= "<?php echo base_url() .'assets/DataTables/datatables.css' ?>">
  <link rel= "stylesheet" type= "text/css" href= "<?php echo base_url() .'assets/css/awesome/css/font-awesome.css' ?>">
  <script type= "text/javascript" src= "<?php echo base_url() .'assets/js/jquery.js' ?>"> </script>
  <script type= "text/javascript" src= "<?php echo base_url() .'assets/js/bootstrap.js' ?>"> </script>
  <script type= "text/javascript" src= "<?php echo base_url() .'assets/DataTables/datatables.js' ?>"> </script>
</head>

  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <a href="admin" class="navbar-brand" href= "<?php echo base_url() .'admin'; ?>">Sistem Informasi Kuisioner Survey</a>
        <button class="navbar-toggle" type="button" data-toggler="collapse" data-target="#navbar-main"
        aria-controls= "navbarText" aria-expanded= "false" arialabel= "Toggle navigation">
        <span class="icon-bar"></span>
      </button>

      <a href= "<?php echo base_url() .'admin'; ?>" class="navbar-brand" > <i class= "fa fa-home"> </i> Dashboard</a></li></a>
      <a href= "<?php echo base_url() ; ?>Datatabel/datamahasiswa" class="navbar-brand" > <i class= "fa fa-tasks"> </i> Data Mahasiswa</a></li></li></a>
      <a href= "<?php echo base_url() ; ?>Datatabel/datapertanyaan" class="navbar-brand" > <i class= "fa fa-inbox"> </i> Data Pertanyaan</a></li></a>
  
      <a href= "<?php echo base_url() ; ?>Datatabel/hasil" class="navbar-brand" > <i class= "fa fa-archive"> </i> Hasil</a></li></a>


      <ul class="nav navbar-nav navbar-right">
        <a href= "<?php echo base_url() .'Admin/logout' ?>" class="navbar-brand my-2 my-sm-0" > <i class= "fa fa-power-off"> </i> Keluar</a></li>></a>
      </ul> 
    </div>
  </div>
</div>



<div class="container col-lg-50">
  <div class="page-header">
    <h2 id="navbar">Data </h2>
  </div>
  <table class="table table-striped" border="1">
    <tr>
      <td>No</td>
      <td>NPM</td>
      <td>Nama Mahasiswa</td>
      <?php for ($i=1; $i <$per+1 ; $i++) { ?>
        <td>Jawaban Pertanyaan <?= $i; ?></td>
      <?php } ?>
      <?php for ($i=1; $i <$peressay+1 ; $i++) { ?>
        <td>Jawaban Essay <?= $i; ?></td>
      <?php } ?>
    </tr>
    <?php 
    if ($n !=null) { ?>
      <?php $no=1; for ($i=0; $i <count($n) ; $i++) { ?>
        <tr>
          <td><?= $no++ ?></td>
          <td><?= $n[$i][0]['npm']; ?></td>
          <td><?= $n[$i][0]['nama']; ?></td>
          <?php for ($c=0; $c <$per ; $c++) { ?>
            <td>
              <?php if (empty($n[$i][$c]['jawaban'])) {
                echo "tadi pertanyaan belum ada";
              } elseif (!empty($n[$i][$c]['jawaban'])) {
                echo $n[$i][$c]['jawaban'];
              } 
              ?>
          </td>
          <?php } ?>
          <?php for ($c=0; $c <$peressay ; $c++) { ?>
            <td>
              <?php if (empty($ne[$i][$c]['jawaban'])) {
                echo "tadi pertanyaan belum ada";
              } elseif (!empty($ne[$i][$c]['jawaban'])) {
                echo $ne[$i][$c]['jawaban'];
              } 
              ?>
          </td>
          <?php } ?>
        </tr>
      <?php } ?>
    <?php } ?>
  </table>
  <p><a href="<?php echo base_url() ;?>Datatabel/deleteAll" class="btn btn-danger btn-lg" onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data..?')">Hapus Semua Hasil Jawaban Responden</a></p>      
</div>

<script src="<?php echo base_url() ;?>/assetshome/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url() ;?>/assetshome/bootstrap/usebootstrap.js"></script>
</body>
</html>
